package com.example.baikt4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<UuDai> uuDaiList = new ArrayList<>();
    private LinearLayoutManager manager;
    private UuDaiAdapter uuDaiAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycleView);
        setUuDaiAdapter();
        recyclerView = findViewById(R.id.recycleView2);
        setUuDaiAdapter();
        recyclerView = findViewById(R.id.recycleView3);
        setUuDaiAdapter();
    }

    public void setUuDaiAdapter()
    {
        uuDaiList.add(new UuDai("Giảm 50%, thèm gì gọi nhé Nhà mang tới liền", "Hòa vào không khí" +
                "siêu sale cuối năm, mời team mình nghỉ tay gọi món yêu thích...", "Chi tiết",
                "https://vuakhuyenmai.vn/wp-content/uploads/2020/09/thecoffeehouse-khuyen-mai-50off-23-9-2020.jpg"));
        uuDaiList.add(new UuDai("Giảm 50%, thèm gì gọi nhé Nhà mang tới liền", "Hòa vào không khí" +
                "siêu sale cuối năm, mời team mình nghỉ tay gọi món yêu thích...", "Chi tiết",
                "https://vuakhuyenmai.vn/wp-content/uploads/2020/09/thecoffeehouse-khuyen-mai-50off-23-9-2020.jpg"));
        uuDaiList.add(new UuDai("Giảm 50%, thèm gì gọi nhé Nhà mang tới liền", "Hòa vào không khí" +
                "siêu sale cuối năm, mời team mình nghỉ tay gọi món yêu thích...", "Chi tiết",
                "https://vuakhuyenmai.vn/wp-content/uploads/2020/09/thecoffeehouse-khuyen-mai-50off-23-9-2020.jpg"));
        uuDaiList.add(new UuDai("Giảm 50%, thèm gì gọi nhé Nhà mang tới liền", "Hòa vào không khí" +
                "siêu sale cuối năm, mời team mình nghỉ tay gọi món yêu thích...", "Chi tiết",
                "https://vuakhuyenmai.vn/wp-content/uploads/2020/09/thecoffeehouse-khuyen-mai-50off-23-9-2020.jpg"));
        uuDaiAdapter = new UuDaiAdapter(MainActivity.this, uuDaiList);
        recyclerView.setAdapter(uuDaiAdapter);
        manager = new LinearLayoutManager(MainActivity.this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(manager);
    }
}