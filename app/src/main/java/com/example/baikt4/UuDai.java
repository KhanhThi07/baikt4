package com.example.baikt4;

public class UuDai {
    String name, desc, detail, image;

    public UuDai(String name, String desc, String detail, String image) {
        this.name = name;
        this.desc = desc;
        this.detail = detail;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
