package com.example.baikt4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.List;

public class UuDaiAdapter extends RecyclerView.Adapter<UuDaiAdapter.UuDaiViewHolder>
{
    private Context context;
    private List<UuDai> uuDaiList;

    public UuDaiAdapter(Context context, List<UuDai> uuDaiList) {
        this.context = context;
        this.uuDaiList = uuDaiList;
    }

    @NonNull
    @Override
    public UuDaiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.card_uudai,null);
        return new UuDaiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UuDaiViewHolder holder, int position) {
        UuDai uuDai = uuDaiList.get(position);
        Glide.with(context)
                .load(uuDai.getImage())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.img_km);

        holder.tv_name.setText(uuDai.getName());
        holder.tv_desc.setText(uuDai.getDesc());
        holder.tv_detail.setText(uuDai.getDetail());
    }

    @Override
    public int getItemCount() {
        return uuDaiList.size();
    }

    public static class  UuDaiViewHolder extends  RecyclerView.ViewHolder{
        TextView tv_name, tv_desc, tv_detail;
        ImageView img_km;
        public UuDaiViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.tv_name);
            tv_desc = itemView.findViewById(R.id.tv_desc);
            tv_detail = itemView.findViewById(R.id.tv_detail);
            img_km = itemView.findViewById(R.id.img_km);
        }
    }
}
